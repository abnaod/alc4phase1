package com.example.alc4phase1

import android.annotation.SuppressLint
import android.net.http.SslError
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.SslErrorHandler
import android.webkit.WebView
import kotlinx.android.synthetic.main.activity_about_alc.*
import android.webkit.WebViewClient


class AboutALCActivity : AppCompatActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_alc)

        val actionbar = supportActionBar
        actionbar!!.title = "About ALC"
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.elevation = 0f

        webViewAbout.settings.javaScriptEnabled = true
        webViewAbout.settings.loadWithOverviewMode = true
        webViewAbout.settings.useWideViewPort = true
        webViewAbout.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }
            override fun onPageFinished(view: WebView, url: String) {
            }
            override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
                handler?.proceed()
            }
        }
        webViewAbout.loadUrl("https://andela.com/alc/")

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
