package com.example.alc4phase1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val actionbar = supportActionBar
        actionbar?.elevation = 0f
        buttonAboutALC.setOnClickListener {
            val activityIntent = Intent(this, AboutALCActivity::class.java)
            startActivity(activityIntent)
        }
        buttonMyProfile.setOnClickListener {
            val activityIntent = Intent(this, MyProfileActivity::class.java)
            startActivity(activityIntent)
        }

    }
}
