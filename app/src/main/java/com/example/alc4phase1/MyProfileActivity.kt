package com.example.alc4phase1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MyProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile)
        val actionbar = supportActionBar
        actionbar?.title = "My Profile"
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.elevation = 0f
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
